#! /usr/bin/python3

import pexpect
import pexpect.replwrap
import sys

c = pexpect.replwrap.REPLWrapper(
        pexpect.spawnu(
            command=sys.argv[1],
            echo=False,
            logfile=sys.stdout,
            timeout=5,
            encoding='utf-8'
        ),
        orig_prompt=sys.argv[2],
        prompt_change=None
    )
with open(sys.argv[3]) as fp:
    for line in fp:
        # c.run_command(line.strip())
        c.run_command(line)
