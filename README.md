# dotnet-sdk

Microsoft .NET https://dotnet.microsoft.com

# F#
* [*Get started with F# with the .NET Core CLI*
  ](https://docs.microsoft.com/en-us/dotnet/fsharp/get-started/get-started-command-line)
  2020-08 Microsoft
* https://fsharp.org/use/linux/

## Install
* [*Install .NET on Linux*
  ](https://docs.microsoft.com/en-US/dotnet/core/install/linux)
  * [*Install the .NET SDK or the .NET Runtime on Fedora*
    ](https://docs.microsoft.com/en-US/dotnet/core/install/linux-fedora)
  * [*Install the .NET SDK or the .NET Runtime on CentOS*
    ](https://docs.microsoft.com/en-US/dotnet/core/install/linux-centos)

# C#
* [*.NET Tutorial - Hello World in 10 minutes*
  ](https://dotnet.microsoft.com/learn/dotnet/hello-world-tutorial/install)
